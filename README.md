Magento SMTP Pro Extension(https://github.com/aschroder/Magento-SMTP-Pro-Email-Extension)
by Ashley Schroder (aschroder.com)

Model/Transports/ 下是预置的各 smtp 供应商设置

Magento 默认是用 OS 自带的 SMTP Server( 通常是 Sendmail 新的是 Postfix ) 发送邮件的，好处是不用做任何设置就可使用，不便之处在于：

1，收件邮箱会显示邮件是由xxx@xxx.xxx.com代发，如图
![](./email-smtp/smtp-0.png)
john表示服务器的名称，host2ez.com是主机商域名，esitediy是账号名称，上面这段表示host2ez.com的代号为john的服务器上的esitediy用户发送。
这样给收件人的感觉就是非常不专业，而且搞不好会被收件服务器当做垃圾邮件来处理。

2，Contact Us页面发送邮件不能给本域名发送，比如我设置发件邮箱为info@esitediy.com，收件邮箱就不能是xxx@esitediy.com ， 邮件收不到。
只能设置一个非本域名邮箱，比如hotmail、 gmail邮箱，给客户回复邮件就要到相应的邮箱去回复，很麻烦。
如果你可以忍受上面两点，那么就不用往下看了。


现在介绍一下通过 SMTP方式，在使用该方式前，请先确认你的邮箱供应商支持 POP/SMTP协议。
腾讯企业邮箱 和 Google Apps的邮箱服务是肯定支持的，已经测试。

解压文件夹，将 app文件夹中的文件全部上传到网站对应的文件夹中
在网站后台刷新缓存，System -> Configuration -> ASCHRODER EXTENSIONS -> SMTP Pro
![](./email-smtp/smtp-1.png)
如图依次选择或者填入 Custom SMTP  、 Login 、 你的企业（域名）邮箱、邮箱的密码 、 hwsmtp.exmail.qq.com （我用的是腾讯企业邮箱，你如果用其他企业邮箱请查看该邮箱的smtp说明）、 465 和SSL。
点击右上角 Save Config保存。
![](./email-smtp/smtp-2.png)

Logging and Debugging 处，点击 Run Self Test按钮。测试成功会显示：
![](./email-smtp/smtp-3.jpg)
![](./email-smtp/smtp-4.png)

ok，再到Contact Us页面发送一封邮件试试。
![](./email-smtp/smtp-5.png)
代发邮箱不见了，并且是sales@esitediy.com给Sales@esitediy.com发送成功了，测试成功。

这个原理类似于在你网站服务器上安装了一个邮件客户端，你的邮件其实是由腾讯邮箱的服务器发送的。










- Free and Opensource email extension for Magento
- Easily send Magento transactional emails via Google Apps, Gmail, Amazon SES or your own SMTP server.
- Test your conifguration from the Magento admin
- View a log of all emails
- Improve deliverability with an external SMTP server


FAQ

Q: It's not working
A: Check for extension conflicts, and check that your host allows outbound SMTP traffic

Q: Does it work with the Mailchimp extension
A: yes, see: http://www.aschroder.com/2011/09/using-smtp-pro-and-ebizmarts-mailchimp-extension-in-magento/

Q: How do I install it manually
A: See: http://www.aschroder.com/2010/05/installing-a-magento-extension-manually-via-ftp-or-ssh/ or use modman.

Q: Self test is failing with "Exception message was: Unable to connect via TLS"
A: Check that you have OpenSSL installed for your PHP environment.

Q: Self test is failing with messages like: "can not open connection to the host, on port 587" or "Connection timed out".
A: Check that you have the SMTP server host and port correct, if you do - then check with your webhost, many block SMTP connections due to spam. If that's the case, there are plenty of expert Magento hosts on display at magespeedtest.com.

Q: Self test is failing with "Exception message was: 5.7.1 Username and Password not accepted. Learn more at 5.7.1..."
A: It's actually good advice to learn more here:  http://support.google.com/mail/bin/answer.py?answer=14257. But two things to check: 
1) that you are really 110% sure you have the right username and password (test it on gmail.com)
2) If that does work, then Google may have blocked your server IP due to too many wrong passwords. You need to log in to gmail.com _from_ that IP - in order to answer the captcha and allow the IP through again. There's a few ways to do that - SOCKS proxy, X forward a browser, use Lynx.

Q: I am getting a Subject set twice error
A: This happens sometimes, typically if it is happening it would also be happening in core Magento. There is a Pull Request [here] (https://github.com/aschroder/Magento-SMTP-Pro-Email-Extension/pull/57) that includes a work around thanks to [Rafael Kassner] (https://github.com/kassner).
